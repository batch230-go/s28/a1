db.hotel.insertOne({
    name: "Single",
    accomodates: 2,
    price: 1000,
    description: "A simple room with all the basic necessities",
    roomsAvailable: 10,
    isAvailable: false
});

db.hotel.insertMany([
    {
        name: "Double",
        accomodates: 3,
        price: 2000,
        description: "A room fit for a small family going on a vacation",
        roomsAvailable: 5,
        isAvailable: false
    },
    {
        name: "Queen",
        accomodates: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple gateway",
        roomsAvailable: 15,
        isAvailable: false
    }
]);

db.hotel.find({name: "Double"});


db.hotel.updateOne(
    {name: "Queen"},
    {
        $set: {
            roomsAvailable: 0,
		}
    }
);


db.hotel.deleteMany({
    roomsAvailable: 0
});
